import unittest
import warnings
from pathways.graph import create_global_graph
from pathways.pathways_relevance import random_means_for_subset
from pathways.kernel import get_kernel
import pandas as pd
import numpy as np
import time


class TestPathwaysRelevanceCase(unittest.TestCase):
    """pathways.kernel test cases"""

    def setUp(self):
        """Create simple graph object"""
        global my_graph, sample_ids, snp_ids, gene_ids, \
            snp_sample_edges, snp_gene_edges, gene_gene_edges, \
            snp_sample_binary, snp_gene_binary, gene_gene_df

        sample_ids = ['sample1','sample2','sample3','sample4','sample5']
        snp_ids = ['snp1','snp2','snp3','snp4']
        gene_ids = ['gene1','gene2','gene3','gene4','gene5']

        snp_sample_edges = [('snp1','sample1'),
                            ('snp2','sample2'),
                            ('snp3','sample3'),
                            ('snp4','sample4'),
                            ('snp4','sample5')]
        snp_gene_edges = [('snp1','gene1'),
                            ('snp2','gene1'),
                            ('snp3','gene2'),
                            ('snp4','gene3')]

        gene_gene_edges = [('gene1','gene5'),
                           ('gene2','gene4'),
                           ('gene3','gene4')]

        data_zeros = np.zeros((len(snp_ids), len(sample_ids)))

        snp_sample_binary = pd.DataFrame(data_zeros, index=snp_ids, columns=sample_ids)
        for snp_sample_edge in snp_sample_edges:
            snp_sample_binary.loc[snp_sample_edge[0],snp_sample_edge[1]] = 1

        data_zeros = np.zeros((len(snp_ids), len(gene_ids)))

        snp_gene_binary = pd.DataFrame(data_zeros, index=snp_ids, columns=gene_ids)
        for snp_gene_edge in snp_gene_edges:
            snp_gene_binary.loc[snp_gene_edge[0],snp_gene_edge[1]] = 1

        gene_gene_df = pd.DataFrame(gene_gene_edges, columns=list('AB'), index=np.arange(0,len(gene_gene_edges)))

        my_graph = create_global_graph(snp_sample=snp_sample_binary,
                                       snp_gene=snp_gene_binary,
                                       gene_gene=gene_gene_df)

    def ignore_warnings(test_func):
        def do_test(self, *args, **kwargs):
            with warnings.catch_warnings():
                warnings.simplefilter("ignore")
                test_func(self, *args, **kwargs)

        return do_test

    def tearDown(self):
        """Teardown."""
        pass

    def test_permutation_scores(self):
        my_graph_kernel = get_kernel(my_graph,method='ed',alpha=0.01)
        sample_gene_submatrix = my_graph_kernel.subset(my_graph.indices(sample_ids),my_graph.indices(gene_ids))
        gene_set_size = 3

        start = time.clock()
        gene_set_permutation_scores = random_means_for_subset(sample_gene_submatrix.todense(), gene_set_size, permutations=10)
        end = time.clock()
        print('Took {} seconds'.format(end - start))
