import unittest
import warnings
from pathways.graph import GlobalGraph, create_global_graph
from igraph import Graph
import numpy as np
import pandas as pd


def check_equal_lists(L1, L2):
    """Check if two lists are equal"""
    if sorted(L1) == sorted(L2):
        return True
    else:
        return False


class TestGraphCase(unittest.TestCase):
    """pathways.graph test cases"""

    def setUp(self):
        """Create simple graph object"""
        global sample_ids, snp_ids, gene_ids, \
            snp_sample_edges, snp_gene_edges, gene_gene_edges, \
            snp_sample_binary, snp_gene_binary, gene_gene_df

        sample_ids = ['sample1','sample2','sample3','sample4','sample5']
        snp_ids = ['snp1','snp2','snp3','snp4']
        gene_ids = ['gene1','gene2','gene3','gene4','gene5']

        snp_sample_edges = [('snp1','sample1'),
                            ('snp2','sample2'),
                            ('snp3','sample3'),
                            ('snp4','sample4'),
                            ('snp4','sample5')]
        snp_gene_edges = [('snp1','gene1'),
                            ('snp2','gene1'),
                            ('snp3','gene2'),
                            ('snp4','gene3')]

        gene_gene_edges = [('gene1','gene5'),
                           ('gene2','gene4'),
                           ('gene3','gene4')]

        data_zeros = np.zeros((len(snp_ids), len(sample_ids)))

        snp_sample_binary = pd.DataFrame(data_zeros, index=snp_ids, columns=sample_ids)
        for snp_sample_edge in snp_sample_edges:
            snp_sample_binary.loc[snp_sample_edge[0],snp_sample_edge[1]] = 1

        data_zeros = np.zeros((len(snp_ids), len(gene_ids)))

        snp_gene_binary = pd.DataFrame(data_zeros, index=snp_ids, columns=gene_ids)
        for snp_gene_edge in snp_gene_edges:
            snp_gene_binary.loc[snp_gene_edge[0],snp_gene_edge[1]] = 1

        gene_gene_df = pd.DataFrame(gene_gene_edges, columns=list('AB'), index=np.arange(0,len(gene_gene_edges)))

    def ignore_warnings(test_func):
        def do_test(self, *args, **kwargs):
            with warnings.catch_warnings():
                warnings.simplefilter("ignore")
                test_func(self, *args, **kwargs)

        return do_test

    def tearDown(self):
        """Teardown."""
        pass

    def test_GlobalGraph_isInstanceOf_igraphGraph(self):
        """
        Test class GlobalGraph creates objects of class Graph from igraph

        """
        g = GlobalGraph()
        assert isinstance(g,Graph)

    def test_create_global_graph(self):
        """
        Test function create_global_graph creates object of class GlobalGraph.

        """

        my_graph = create_global_graph(snp_sample=snp_sample_binary,
                                       snp_gene=snp_gene_binary,
                                       gene_gene=gene_gene_df)

        assert isinstance(my_graph,GlobalGraph)

    def test_samples_names(self):
        """
        Test method samples_names from class GlobalGraph returns list of names for edges of type 'sample'.

        """
        my_graph = create_global_graph(snp_sample=snp_sample_binary,
                                       snp_gene=snp_gene_binary,
                                       gene_gene=gene_gene_df)

        assert check_equal_lists(my_graph.samples_names, sample_ids)

    def test_snps_names(self):
        """
        Test method 'snps_names' from class GlobalGraph returns list of names for edges of type 'snp'.

        """

        my_graph = create_global_graph(snp_sample=snp_sample_binary,
                                       snp_gene=snp_gene_binary,
                                       gene_gene=gene_gene_df)

        assert check_equal_lists(my_graph.snps_names, snp_ids)

    def test_genes_names(self):
        """
        Test method 'genes_names' from class GlobalGraph returns list of names for edges of type 'gene'.

        """
        my_graph = create_global_graph(snp_sample=snp_sample_binary,
                                       snp_gene=snp_gene_binary,
                                       gene_gene=gene_gene_df)

        assert check_equal_lists(my_graph.genes_names, gene_ids)

    def test_samples_indices(self):
        """
        Test method 'samples_indices' from class GlobalGraph returns list of indices for edges of type 'sample'

        """
        my_graph = create_global_graph(snp_sample=snp_sample_binary,
                                       snp_gene=snp_gene_binary,
                                       gene_gene=gene_gene_df)

        samples_indices = np.arange(0,len(sample_ids))

        assert check_equal_lists(my_graph.samples_indices,samples_indices)

    def test_snps_indices(self):
        """
        Test method 'snps_indices', from class GlobalGraph, returns list of indices for edges of type 'snp'

        """
        my_graph = create_global_graph(snp_sample=snp_sample_binary,
                                       snp_gene=snp_gene_binary,
                                       gene_gene=gene_gene_df)

        snps_indices = np.arange(len(sample_ids),len(sample_ids)+len(snp_ids))

        assert check_equal_lists(my_graph.snps_indices,snps_indices)

    def test_genes_indices(self):
        """
        Test method 'genes_indices', from class GlobalGraph, returns list of indices for edges of type 'gene'.

        """
        my_graph = create_global_graph(snp_sample=snp_sample_binary,
                                       snp_gene=snp_gene_binary,
                                       gene_gene=gene_gene_df)

        genes_indices = np.arange(len(sample_ids)+len(snp_ids),
                                  len(sample_ids)+len(snp_ids)+len(gene_ids))

        assert check_equal_lists(my_graph.genes_indices,genes_indices)

    def test_indices(self):
        """
        Test method 'samples_indices' from class GlobalGraph returns list of indices for a list of edge names.

        """
        my_graph = create_global_graph(snp_sample=snp_sample_binary,
                                       snp_gene=snp_gene_binary,
                                       gene_gene=gene_gene_df)

        samples_indices = np.arange(0,len(sample_ids))

        assert check_equal_lists(my_graph.indices(names=sample_ids), samples_indices)







