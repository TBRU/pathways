import unittest
import warnings
from pathways.kernel import get_kernel, expm_eig, expm_solveq, ed, led, \
    normalize_kernel, center_kernel, to_distance, is_sequence, \
    subset_csc
from igraph import Graph
import numpy as np
from numpy import allclose
from scipy.sparse import linalg
from scipy.sparse import csc_matrix
import time


class TestKernelCase(unittest.TestCase):
    """pathways.kernel test cases"""

    def setUp(self):
        """Create simple graph object"""
        global my_graph, my_graph_500

        my_graph = Graph([(0,1), (0,2), (2,3), (3,4), (4,2), (2,5), (5,0), (6,3), (5,6)])
        my_graph_500 = Graph.Erdos_Renyi(n=500,p=0.3,directed=False)

    def ignore_warnings(test_func):
        def do_test(self, *args, **kwargs):
            with warnings.catch_warnings():
                warnings.simplefilter("ignore")
                test_func(self, *args, **kwargs)

        return do_test

    def tearDown(self):
        """Teardown."""
        pass

    def test_expm_eig_vs_expm_solveq_in_speed_1000nodes(self):
        my_graph_1000 = Graph.Erdos_Renyi(n=1000, p=0.3, directed=False)
        print('Graph nr edges: {}'.format(my_graph_1000.ecount()))
        print('Graph nr edges: {}'.format(my_graph_1000.vcount()))

        my_graph_1000_adj = csc_matrix(my_graph_1000.get_adjacency().data)

        time_start = time.clock()
        my_kernel_expm_solveq = expm_solveq(my_graph_1000_adj.todense()*0.01)
        time_end = time.clock()
        print('expm_solveq time:{}'.format(time_end - time_start))

        time_start_a = time.clock()
        my_kernel_expm_eig = expm_eig(my_graph_1000_adj.todense() * 0.01)
        time_end_a = time.clock()
        print('expm_eig time:{}'.format(time_end_a - time_start_a))

        assert allclose(my_kernel_expm_solveq, my_kernel_expm_eig)


    def test_expm_eig_vs_expm_pade_in_sparse_500nodes(self):

        print('Graph nr edges: {}'.format(my_graph_500.ecount()))
        print('Graph nr edges: {}'.format(my_graph_500.vcount()))
        my_graph_500_adj = csc_matrix(my_graph_500.get_adjacency().data)

        time_start = time.clock()
        my_kernel_expm_eig = expm_eig(my_graph_500_adj.todense()*0.01)
        time_end = time.clock()
        print('expm_eig time:{}'.format(time_end - time_start))

        time_start_a = time.clock()
        my_kernel_expm_pade = linalg.expm(my_graph_500_adj*0.01)
        time_end_a = time.clock()
        print('expm_pade time:{}'.format(time_end_a - time_start_a))

        assert allclose(my_kernel_expm_eig, my_kernel_expm_pade.A)

    def test_ed_vs_get_kernel_with_sparse(self):
        my_graph_500_adj = csc_matrix(my_graph_500.get_adjacency().data)

        time_start = time.clock()
        my_kernel_ed = ed(my_graph_500_adj, alpha=0.01)
        print('kernel with function "ed":{}'.format(time.clock() - time_start))
        time_start_a = time.clock()

        my_kernel_ed_using_get_kernel = get_kernel(my_graph_500,method='ed', alpha=0.01).matrix
        print('kernel with function "get_kernel":{}'.format(time.clock() - time_start_a))

        assert allclose(my_kernel_ed.A, my_kernel_ed_using_get_kernel.A)

    def test_led_vs_get_kernel_with_sparse(self):
        my_graph_500_lap = csc_matrix(my_graph_500.laplacian())
        time_start = time.clock()
        my_kernel_led = led(my_graph_500_lap, alpha=0.01)
        print('kernel with function "led":{}'.format(time.clock() - time_start))
        time_start_a = time.clock()
        my_kernel_led_using_get_kernel = get_kernel(my_graph_500,method='led', alpha=0.01).matrix
        print('kernel with function "get_kernel":{}'.format(time.clock() - time_start_a))

        assert allclose(my_kernel_led.A, my_kernel_led_using_get_kernel.A)

    def test_normalize_kernel_inplace(self):
        my_graph_kernel = get_kernel(my_graph,method='ed',alpha=0.01)
        my_graph_kernel_normalized_matrix = normalize_kernel(my_graph_kernel.matrix)
        my_graph_kernel.normalize(inplace=True)
        assert allclose(my_graph_kernel.matrix.A, my_graph_kernel_normalized_matrix.A)

    def test_normalize_kernel_property(self):
        """ K is normalized if and only if Kii = 1 para todo i en {1, ..., n}.
        """
        my_graph_kernel = get_kernel(my_graph, method='ed', alpha=0.01)
        my_graph_kernel_n = my_graph_kernel.normalize(inplace=False)
        ones_vector = np.ones(my_graph_kernel_n.shape[0])

        assert allclose(np.diag(my_graph_kernel_n.matrix.todense()), ones_vector)


    def test_center_kernel_property(self):
        """ A symmetric kernel matrix K is centered if and only if Ke = 0.
        Where 'e' is a column vector all of whose elements are 1 (e = [1,1,...,1].T) and
        '0' is a column vector all of whose elements are zero.
        """
        my_graph_kernel = get_kernel(my_graph, method='ed', alpha=0.01)
        my_graph_kernel_c = my_graph_kernel.center(inplace=False)
        e_vector = np.ones((my_graph_kernel.shape[0], 1))
        zero_vector = np.zeros((my_graph_kernel.shape[0], 1))

        #print(my_graph_kernel_c.matrix.dot(e_vector))
        #print(zero_vector)

        assert allclose(my_graph_kernel_c.matrix.dot(e_vector), zero_vector)

    def test_center_kernel_inplace(self):
        my_graph_kernel = get_kernel(my_graph,method='ed',alpha=0.01)
        my_graph_kernel_centered_matrix = center_kernel(my_graph_kernel.matrix)
        my_graph_kernel.center(inplace=True)
        assert allclose(my_graph_kernel.matrix.A, my_graph_kernel_centered_matrix.A)

    def test_as_distance_kernel_inplace(self):
        my_graph_kernel = get_kernel(my_graph,method='ed',alpha=0.01)
        my_graph_kernel_distances = to_distance(my_graph_kernel.matrix,is_normalized=False)
        my_graph_kernel.as_distance(inplace=True)
        assert allclose(my_graph_kernel.matrix.A, my_graph_kernel_distances.A)

    def test_GraphKernel_head(self):
        my_graph_kernel = get_kernel(my_graph, method='ed', alpha=0.01)
        assert my_graph_kernel.head(n=5).shape == (5,5)

    def test_is_sequence_when_is_true(self):
        some_array = [5,6,7,8]
        assert is_sequence(some_array)

    def test_is_sequence_when_is_false(self):
        some_array = [5,8,3,2]
        assert not is_sequence(some_array)

    def test_subset_csc_with_rowIndices_as_sequence(self):
        some_csc_matrix = csc_matrix([[1, 2, 0], [0, 0, 3], [4, 0, 5], [1,2,3]])
        row_indices = [1,2,3]
        col_indices = [0,2]
        expected_output = csc_matrix([[0,3],[4,5],[1,3]])
        output = subset_csc(some_csc_matrix,rows_indices=row_indices,cols_indices=col_indices)
        assert allclose(expected_output.A, output.A)

    def test_subset_csc_with_colIndices_as_sequence(self):
        some_csc_matrix = csc_matrix([[1, 2, 0], [0, 0, 3], [4, 0, 5], [1,2,3]])
        row_indices = [0,2]
        col_indices = [0,1,2]
        expected_output = csc_matrix([[1,2,0],[4,0,5]])
        output = subset_csc(some_csc_matrix,rows_indices=row_indices,cols_indices=col_indices)
        assert allclose(expected_output.A, output.A)

    def test_subset_with_rows_and_columns_not_as_sequence(self):
        some_csc_matrix = csc_matrix([[1, 2, 0], [0, 0, 3], [4, 0, 5], [1,2,3]])
        row_indices = [1,3]
        col_indices = [0,2]
        expected_output = csc_matrix([[0,3],[1,3]])
        output = subset_csc(some_csc_matrix,rows_indices=row_indices,cols_indices=col_indices)
        assert allclose(expected_output.A, output.A)

    def test_subset_with_rows_and_columns_as_sequence(self):
        some_csc_matrix = csc_matrix([[1, 2, 0], [0, 0, 3], [4, 0, 5], [1,2,3]])
        row_indices = [1,2,3]
        col_indices = [0,1,2]
        expected_output = csc_matrix([[0,0,3],[4,0,5],[1,2,3]])
        output = subset_csc(some_csc_matrix,rows_indices=row_indices,cols_indices=col_indices)
        assert allclose(expected_output.A, output.A)

    def test_subset_csc_when_IndexError_in_rows(self):
        some_csc_matrix = csc_matrix([[1, 2, 0], [0, 0, 3], [4, 0, 5], [1,2,3]])
        row_indices = [2,4]
        col_indices = [0,1,2]

        with self.assertRaises(IndexError):
            subset_csc(some_csc_matrix,row_indices,col_indices)

    def test_subset_csc_when_IndexError_in_cols(self):
        some_csc_matrix = csc_matrix([[1, 2, 0], [0, 0, 3], [4, 0, 5], [1,2,3]])
        row_indices = [1,2,3]
        col_indices = [0,3]

        with self.assertRaises(IndexError):
            subset_csc(some_csc_matrix,row_indices,col_indices)

    def test_GraphKernel_subset(self):
        my_graph_kernel = get_kernel(my_graph, method='ed', alpha=0.01)
        row_indices = [0,1,2,3]
        col_indices = [0,1,2]
        print(my_graph_kernel.shape)
        assert allclose(my_graph_kernel.subset(rows=row_indices,cols=col_indices).A,
                        subset_csc(my_graph_kernel.matrix,rows_indices=row_indices,cols_indices=col_indices).A)


