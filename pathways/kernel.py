"""
    pathways.kernel


        This module implements the :class:`GraphKernel` object.


        :copyright: (c) 2018 by Monica Ticlla.
        :license: MIT, see LICENSE for more details.

"""

import numpy as np
from numpy.matlib import repmat
# import subprocess
# from igraph.datatypes import Matrix
from scipy.sparse import csc_matrix,lil_matrix,issparse
# from scipy.sparse import linalg
from scipy.linalg import inv,eigh,solve



__author__ = "Monica Ticlla"
__email__ = "monica.ticlla@sib.swiss"
__copyright__ = "Copyright (C) 2018 Monica Ticlla"
__license__ = "MIT see LICENSE for more details"


class GraphKernel(object):
    """Generic Kernel of a graph.

    This class is built to provide an interface for handling the Kernel matrix of :class:`GlobalGraph` object.

    """
    import warnings
    warnings.filterwarnings("always")

    def __init__(self, *args, **kwds):
        """__init__(data=None, vertices=None, method=None, alpha=None,
                    centered=False, normalized=False, as_distance=False)

        Constructs the kernel of a graph.

        Args:

         matrix: the elements of the matrix as a list of lists or C{None} to create a 0x0 matrix.
         vertices: the vertices of the graph as a C{igraph.VertexSeq} object.
         method:
         alpha:
         centered:
         normalized:
         as_distance:

        """
        # Set up default values for the parameters. This should match the order in *args
        kwd_order = ["matrix", "vertices", "method", "alpha", "centered", 'normalized','as_distance']
        params = [[],None,None,None,False,False,False]

        args = list(args)

        # Override default parameters from args
        params[:len(args)] = args
        # Override default parameters from keywords
        for idx, k in enumerate(kwd_order):
            if k in kwds:
                params[idx] = kwds[k]
        # Now, translate the params list to argument names
        matrix, vertices, method, alpha, centered, normalized, as_distance = params

        self.matrix = matrix
        self.vertices = vertices
        self.__attributes = dict()
        self.__attributes['method'] = method
        self.__attributes['alpha'] = alpha
        self.__attributes['centered'] = centered
        self.__attributes['normalized'] = normalized
        self.__attributes['as_distance'] = as_distance

    @property
    def matrix(self):
        """Returns the kernel matrix as a sparse matrix"""

        return self.__matrix

    @matrix.setter
    def matrix(self, data=None):
        """Sets the data stored in the matrix as a sparse matrix"""
        if data is not None:
            if not issparse(data):
                self.__matrix = csc_matrix(data)
            elif issparse(data):
                self.__matrix = data

    @property
    def shape(self):
        return self.__matrix.shape

    @property
    def alpha(self):
        return self.__attributes['alpha']

    @property
    def method(self):
        return self.__attributes['method']
    @property
    def normalized(self):
        return self.__attributes['normalized']

    @property
    def attributes(self):
        return self.__attributes

    def center(self, inplace=True):
        if inplace:
            self.matrix = center_kernel(self.matrix)
            self.__attributes['centered'] = True
        elif not inplace:
            return GraphKernel(center_kernel(self.matrix),
                               vertices=self.vertices,
                               centered=True, alpha=self.alpha,
                               method=self.method)

    def normalize(self, inplace=True):
        if inplace:
            self.matrix=normalize_kernel(self.matrix)
            self.__attributes['normalized'] = True
        elif not inplace:
            return GraphKernel(normalize_kernel(self.matrix),
                               vertices=self.vertices,
                               normalized=True, alpha=self.alpha,
                               method=self.method)

    def as_distance(self, inplace=True):
        # EE contains the squared distances
        if not self.normalized:
            EE = to_distance(self.matrix, is_normalized=False)
        elif self.normalized:
            EE = to_distance(self.matrix, is_normalized=True)

        if inplace:
            self.matrix = EE
            self.__attributes['as_distance'] = True
        elif not inplace:
            return GraphKernel(EE, vertices=self.vertices,
                               as_distance=True, alpha=self.alpha,
                               method=self.method)

    def head(self, n=5, m=5):
        """Get the first n rows and m columns from self.

        Args:
            n (int): optional
                  The number of rows to get. This number must be
                  greater than 0. If not specified, 5 rows will be retrieved.
            m (int): optional
                  The number of columns (samples) to get. This number must be
                  greater than 0. If not specified, 5 columns will be retrieved.

        Returns:
            A subset of the kernel matrix in dense format

        """
        if self.__matrix.shape[0]>=5:
            return self.__matrix[:n,:m].todense()
        elif self.__matrix.shape[0]<5:
            return self.matrix.todense()

    def subset(self, rows, cols):
        """Subsets the kernel matrix according to given indices for rows and columns.

        Args:
            rows: list with numerical indices of the Kernel matrix
            cols: list with numerical indices of the kernel matrix

        Returns:
            A subset of the kernel matrix in sparse format

        """
        return subset_csc(self.matrix,rows_indices=rows,cols_indices=cols)


def expm_eig(A):
    """Computes the matrix exponential for a square, symmetric matrix, by eigenvalue decomposition.

    Args:
        A: a square and symmetric matrix

    """
    D, V = eigh(A)
    #np.exp(D) * V is equivalent to V @ np.diag(D)
    return np.dot(np.exp(D) * V, inv(V))


def expm_solveq(A):
    """Computes the matrix exponential for a square, symmetric matrix, by eigenvalue decomposition.

    """
    D, V = eigh(A)
    return solve(V.T, (np.exp(D) * V).T).T


def ed(A,alpha=0.1):
    """Computes the exponential diffusion kernel (ed).

    Args:
        A: Adjacency matrix of a graph in sparse format.
        alpha: the diffusion time from node i to node j. Values are between 0 and 1

    Returns: The exponential diffusion kernel matrix in sparse format.

    """

    return csc_matrix(expm_solveq(alpha * A.todense()))


def led(L,alpha=0.1):
    """Compute the Laplacian exponential diffusion kernel.

    Args:
        L    : The Laplacian matrix of a graph in sparse format.
        alpha: The diffusion time from node i to node j. Values are between 0 and 1
    Returns:
        The Laplacian exponential diffusion kernel in sparse format.

    """
    return csc_matrix(expm_solveq(-alpha * L.todense()))


def normalize_kernel(K):
    """Normalizes the kernel matrix of a graph.

    'K' is normalized if and only if Kii = 1 para todo i en {1, ..., n}.

    Args:
        K: The kernel matrix of a graph in sparse format.
    Returns:
        The normalized (i.e cosine similarity) kernel matrix in sparse format

    """
    n = K.shape[0]
    # Row vector
    D = np.diag(K.todense())
    #
    Kii = repmat(D, n, 1)
    Kjj = repmat(np.reshape(D, (n, -1)), 1, n)

    return csc_matrix(K.todense() / (np.sqrt(Kii * Kjj)))


def center_kernel(K):
    """Centers the kernel matrix of a graph.

    A centered kernel matrix corresponds to inner products of centered node vectors if and only if Ke =0.
    e is a column vector, all of whose elements are 1 (e = [1,1,1, ..., 1].T).

    Args:
         K: The kernel matrix of a graph in sparse format.
    Returns:
        The centered kernel matrix in sparse format

    """
    n = K.shape[0]
    K_row_means = K.mean(axis=0)
    ones_column_vector = np.ones((n, 1))

    # This is a product of a column vector by row vector
    # creates a matrix with identical rows, each being the vector of row means of K
    K_row_means_n_times = ones_column_vector * K_row_means

    # mean of row means
    mean_of_K_row_means = K_row_means.mean()

    # the centered kernel
    C = K.todense() - K_row_means_n_times.T - K_row_means_n_times + mean_of_K_row_means * np.ones((n, n))

    return csc_matrix(C)


def to_distance(K, is_normalized=False):
    """Derives the distance measure associated to the kernel matrix K.

    Args:
        K: The kernel matrix of a graph in sparse format.
    Returns:
        The squared distances in a sparse format matrix

    """
    if not is_normalized:
        n = K.shape[0]
        # Row vector
        D = np.diag(K.todense())
        #
        Kii = repmat(D, n, 1)
        Kjj = repmat(np.reshape(D, (n, -1)), 1, n)
        EE = Kii + Kjj - 2 * np.array(K.todense())
    elif is_normalized:
        EE = 2 * (1 - np.array(K.todense()))

    EE[EE < 0.0] = 0.0

    return csc_matrix(EE)


def is_sequence(v):
    """Check if array C{v} is a sequence of numbers.

    Args:
        v: Array of numbers
    Returns: bool
    """
    v_start = v[0]
    v_end = v[-1]
    return np.array_equal(v, np.arange(v[0],v[-1] + 1))


def subset_csc(M, rows_indices,cols_indices):
    """Subset a CSC sparse matrix 'M' given the specified indices for rows (rows_indices) and columns (cols_indices).

    Args:
        M: The CSC matrix
        rows_indices:
        cols_indices:
    Returns: A subset of M in CSC matrix format

    """
    if is_sequence(rows_indices) & is_sequence(cols_indices):
        row_start_ix = rows_indices[0]
        row_end_ix = rows_indices[-1] + 1
        col_start_ix = cols_indices[0]
        col_end_ix = cols_indices[-1] + 1

        new_M = M[row_start_ix:row_end_ix,col_start_ix:col_end_ix]
        return new_M

    elif is_sequence(rows_indices) & (not is_sequence(cols_indices)):
        row_start_ix = rows_indices[0]
        row_end_ix = rows_indices[-1] + 1
        try:
            # May raise IndexError
            new_M = M[row_start_ix:row_end_ix,cols_indices]
        except IndexError as e:
            print(e)
            raise e
        else:
            return new_M

    elif (not is_sequence(rows_indices)) & is_sequence(cols_indices):
        col_start_ix = cols_indices[0]
        col_end_ix = cols_indices[-1] + 1
        try:
            # May raise IndexError
            new_M = M[rows_indices, col_start_ix:col_end_ix]
        except IndexError as e:
            print(e)
            raise e
        else:
            return new_M

    elif (not is_sequence(rows_indices)) & (not is_sequence(cols_indices)):
        try:
            # May raise IndexError
            new_M = M[rows_indices,:][:,cols_indices]
        except IndexError as e:
            print(e)
            raise e
        else:
            return new_M


def get_kernel(graph, method='ed', alpha=0.01):
    """Compute the kernel matrix from an adjacency matrix.

    Args:
        graph: :class:`Graph` or :class:`GlobalGraph` object
        method: any element from this list ['ed', 'led','vnd']
            - ed: The Exponential Diffusion kernel
            - led: The Laplacian Exponential Diffusion kernel
            - vnd: The Von Neumann Diffusion kernel
        alpha: diffusion time from node i to node j

    Returns:
        A :class:`GraphKernel` object

    """

    # The exponential diffusion kernel
    if method == 'ed':
        # 1. Get the adjacency matrix of the graph
        A = csc_matrix(graph.get_adjacency().data)
        # 2. Compute the matrix exponential using eigenvalue decomposition
        K = ed(A, alpha)

    # The Laplacian exponential diffusion kernel
    elif method == 'led':
        # 1. Get the Laplacian matrix of the graph
        L = csc_matrix(graph.laplacian())
        # 2. Compute matrix exponential
        K = led(L, alpha)

    # The von Neumann diffusion kernel
    elif method == 'vnd':
        # 1. Get the adjacency matrix of the graph
        A = lil_matrix(graph.get_adjacency().data)
        # 2.
        A= -alpha * A
        # 3. This is equivalent to adding the Identity matrix
        A.setdiag(1)
        #4
        K = inv(A.tocsc)

    K[K<0.0] = 0.0

    return GraphKernel(K, vertices=graph.vs, alpha=alpha, method=method)
