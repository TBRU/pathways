"""
    pathways.graph


        This module implements the :class:`GlobalGraph` object.


        :copyright: (c) 2018 by Monica Ticlla.
        :license: MIT, see LICENSE for more details.

"""


from igraph import Graph


__author__ = "Monica Ticlla"
__email__ = "monica.ticlla@sib.swiss"
__copyright__ = "Copyright (C) 2018 Monica Ticlla"
__license__ = "MIT see LICENSE for more details"


class GlobalGraph(Graph):
    """
    The :class:`GlobalGraph` object is a subclass of :class:`igraph.Graph` and provides an interface for
    handling the `igraph.Graph <http://igraph.org/python/doc/igraph.Graph-class.html>`_ object created with
    :py:meth:`~pathways.graph.create_global_graph`. A :class:`GlobalGraph` object has three types of nodes or
    vertices ('snp', 'sample', 'gene'). Edges in the :class:`GlobalGraph` are undirected and connect a node of type 'snp'
    with a node of type sample or with a node of type 'gene'. Edges are also connecting vertices of type 'gene' to represent
    gene-to-gene interactions.

    A :class:`GlobalGraph` object is created only by :py:meth:`~pathways.graph.create_global_graph` function.

    """

    @property
    def samples_names(self):
        """Finds all nodes of type 'sample' in the graph and returns their names.

        Returns:
            A list with the names of 'sample' nodes.

        """
        samples = self.vs.select(type_eq = 'sample')['name']
        return samples

    @property
    def snps_names(self):
        """Finds all nodes of type 'snp' in the graph and returns their names.

        Returns:
            A list with the names of SNP nodes in the graph.

        """
        snps = self.vs.select(type_eq = 'snp')['name']
        return snps

    @property
    def genes_names(self):
        """Finds all nodes of type 'gene' in the graph and returns their names.

        Returns:
            List of names of gene nodes in the graph.

        """
        genes = self.vs.select(type_eq = 'gene')['name']
        return genes

    @property
    def samples_indices(self):
        """Finds all nodes of type 'sample' in the graph and returns their numerical indices.

        Returns:
            List of the indices of sample nodes in the  graph.

        """

        samples_ixs = [v.index for v in self.vs.select(type_eq = 'sample')]
        return samples_ixs

    @property
    def snps_indices(self):
        """Finds all nodes of type 'snp' in the graph and returns their numerical indices.

        Returns:
            A List of indices of SNP nodes in the graph.

        """
        snps_ixs = [v.index for v in self.vs.select(type_eq = 'snp')]
        return snps_ixs

    @property
    def genes_indices(self):
        """Finds all nodes of type 'gene' in the graph and returns their numerical indices.

        Returns:
            A list of indices of genes nodes in the graph.

        """
        genes_ixs = [v.index for v in self.vs.select(type_eq = 'gene')]
        return genes_ixs

    def indices(self, names):
        """Given the list of node names, finds these nodes and returns their indices in the :class:`Graph` object.

        Args:
            names (list): list of vertices names

        Returns:
            A list of indices for the given nodes names

        """
        # TODO(monica.ticlla@sib.swiss): Raise warning when no nodes with a name in names is found in the :class:`Graph` object.

        indices = [v.index for v in self.vs.select(name_in = names)]
        return indices


def create_global_graph(snp_sample, snp_gene, gene_gene):
    """Creates an instance of :class:`GlobalGraph`.

    Args:
        snp_sample (:class:`pandas.DataFrame` object): A pandas :class:`DataFrame` object with 'SNPs' as rows and 'samples'
            as columns. Labels of rows should be the names of the 'SNPs' and labels of columns should be the names of
            the 'samples'. Duplicates are not allowed. For a given row and column index, the value of the entry is '1'
            if the 'SNP' is present in that 'sample' or '0' otherwise.
        snp_gene (:class:`pandas.DataFrame` object): A pandas :class:`DataFrame` object with 'SNPs' as rows and 'genes'
            as columns. Labels of rows should be the names of the 'SNPs' and labels of columns should be the names of
            the 'genes'. The number, labels and order of rows should be identical to those of snp_sample.
            For a given row and column index, the value of the entry is '1' if the 'SNP' is present in that 'gene' or
            '0' otherwise.
        gene_gene (:class:`pandas.DataFrame` object): A pandas :class:`DataFrame` object with two columns. Each row represents
            the interaction of two genes, and the two columns contain the name of the interacting genes.

    Example: ::

        from pathways import graph
        import numpy as np
        import pandas as pd


        sample_ids = ['sample1','sample2','sample3','sample4','sample5']
        snp_ids = ['snp1','snp2','snp3','snp4']
        gene_ids = ['gene1','gene2','gene3','gene4','gene5']

        snp_sample_edges = [('snp1','sample1'),
                            ('snp2','sample2'),
                            ('snp3','sample3'),
                            ('snp4','sample4'),
                            ('snp4','sample5')]
        snp_gene_edges = [('snp1','gene1'),
                            ('snp2','gene1'),
                            ('snp3','gene2'),
                            ('snp4','gene3')]

        gene_gene_edges = [('gene1','gene5'),
                           ('gene2','gene4'),
                           ('gene3','gene4')]

        data_zeros = np.zeros((len(snp_ids), len(sample_ids)))

        snp_sample_binary = pd.DataFrame(data_zeros, index=snp_ids, columns=sample_ids)
        for snp_sample_edge in snp_sample_edges:
            snp_sample_binary.loc[snp_sample_edge[0],snp_sample_edge[1]] = 1

        data_zeros = np.zeros((len(snp_ids), len(gene_ids)))

        snp_gene_binary = pd.DataFrame(data_zeros, index=snp_ids, columns=gene_ids)
        for snp_gene_edge in snp_gene_edges:
            snp_gene_binary.loc[snp_gene_edge[0],snp_gene_edge[1]] = 1

        gene_gene_df = pd.DataFrame(gene_gene_edges, columns=list('AB'), index=np.arange(0,len(gene_gene_edges)))

        my_global_graph = graph.create_global_graph(snp_sample=snp_sample_binary,
                                                    snp_gene=snp_gene_binary,
                                                    gene_gene=gene_gene_df)

    """

    snp_sample_adj = snp_sample
    snp_gene_adj = snp_gene

    ##################################################################################
    # Clean datasets
    ##################################################################################
    # Remove samples with no SNPs
    samples_noSNPs = snp_sample_adj.columns.values[snp_sample_adj.sum(axis=0) == 0]
    snp_sample_adj.drop(samples_noSNPs, axis=1, inplace=True)

    snps_todrop = list()
    # Remove SNPs with zero frequency
    snps_noSamples = list(snp_sample_adj.index.values[snp_sample_adj.sum(axis=1) == 0])
    if len(snps_noSamples)>0:
        snps_todrop.extend(snps_noSamples)
    # Remove SNPs not assigned to a gene
    snps_noGene = list(snp_gene_adj.index.values[snp_gene_adj.sum(axis=1) == 0])
    if len(snps_noGene)>0:
        snps_todrop.extend(snps_noGene)
    # put together SNPs to drop
    if len(snps_todrop) > 0:
        snps_todrop = set(snps_todrop)
        snp_sample_adj.drop(snps_todrop, axis=0, inplace=True)
        snp_gene_adj.drop(snps_todrop, axis=0, inplace=True)

    ##################################################################################
    # List of vertices for graph
    ##################################################################################
    samples_names = snp_sample_adj.columns.values
    snps_names= snp_sample_adj.index.values

    genes_names = list()
    genes_names.extend(snp_gene_adj.columns.values)
    genes_names.extend(gene_gene.iloc[:,0].values)
    genes_names.extend(gene_gene.iloc[:,1].values)
    genes_names = list(set(genes_names))


    ##################################################################################
    # Create graph
    ##################################################################################

    # Add vertices
    #---------------

    global_graph = GlobalGraph()
    # First add vertices of type 'sample'
    global_graph.add_vertices(samples_names)
    global_graph.vs['type'] = 'sample'
    # Then, add nodes of type 'snp'
    global_graph.add_vertices(snps_names)
    global_graph.vs.select(name_in = snps_names)['type'] = 'snp'
    # Finally add nodes of type 'gene'
    global_graph.add_vertices(genes_names)
    global_graph.vs.select(name_in = genes_names)['type'] = 'gene'

    # Add edges
    #---------------
    snp_sample_edges = []
    snp_gene_edges = []
    gene_gene_edges = zip(gene_gene.iloc[:,0].values,gene_gene.iloc[:,1].values)

    for snp in snps_names:
        sample_ids = snp_sample_adj.columns.values[snp_sample_adj.loc[snp,:] == 1]
        gene_ids = snp_gene_adj.columns.values[snp_gene_adj.loc[snp,:] == 1]

        snp_ids_for_samples = [snp]*len(sample_ids)
        snp_ids_for_genes = [snp]*len(gene_ids)

        snp_sample_edges.extend(zip(snp_ids_for_samples,sample_ids))
        snp_gene_edges.extend(zip(snp_ids_for_genes,gene_ids))

    global_graph.add_edges(snp_sample_edges)
    global_graph.add_edges(snp_gene_edges)
    global_graph.add_edges(gene_gene_edges)

    # Check graph is connected
    # if  not global_graph.is_connected():
    return global_graph




