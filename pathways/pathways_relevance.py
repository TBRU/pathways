"""
    pathways.pathways_relevance


        This module implements the :class:`PathwaysScores` object.


        :copyright: (c) 2018 by Monica Ticlla.
        :license: MIT, see LICENSE for more details.

"""

# from igraph import Graph
import numpy as np
import pandas as pd
import random
from itertools import chain
import matplotlib.pyplot as plt
from pathways import util

__author__ = "Monica Ticlla"
__email__ = "monica.ticlla@sib.swiss"
__copyright__ = "Copyright (C) 2018 Monica Ticlla"
__license__ = "MIT see LICENSE for more details"


class PathwaysScores(object):
    """This class is built to provide an interface for handling the list of pathways and their scores computed by
    :py:meth:`score_pathways`.

    """
    def __init__(self, *args, **kwds):
        """__init__(pathways, names, scores, p_scores, gene_scores)

        Constructs the PathwaysScores object.

        Args:
            pathways   : dictionary of pathways with list of genes as values for each pathway key
            names      : dictionary of pathways with the long names of each pathway as values
            scores     : dictionary of pathways with raw relevance score as value for each pathway key
            p_scores   : dictionary of pathways with the probability of its raw relevance score as value
            genes_scores: dictionary of pathways with dictionary of raw scores for each gene


        """

        # Set up default values for the parameters. This should match the order in *args
        kwd_order = ['pathways','names','scores','p_scores','genes_scores']
        params = [{},{},{},{},{}]

        args = list(args)

        # Override default parameters from args
        params[:len(args)] = args
        # Override default parameters from keywords
        for idx, k in enumerate(kwd_order):
            if k in kwds:
                params[idx] = kwds[k]
        # Now, translate the params list to argument names
        pathways, names, scores, p_scores, genes_scores = params

        self.pathways = pathways
        self.names = names
        self.scores = scores
        self.p_scores = p_scores
        self.genes_scores = genes_scores

    def __get_sorted_pscores(self, threshold=1.0):
        """Sort self.p_scores

        Args:
            threshold: maximum value for the pscore of a pathway.
                       Pathways with pscores above this threshold ar not considered.

        Returns:
            A sorted list of tuples with each element having this form '(p_score, pathway_id)'

        """
        sorted_pscores = sorted((pscore, pathway) for (pathway, pscore) in self.p_scores.items() if pscore <= threshold)
        return sorted_pscores

    def significant_pathways(self, threshold=0.05):
        """Retrieves IDs of pathways with pscore bellow threshold.

        Args:
            threshold: maximum value for the pscore of a pathway
        Returns:
            List of IDs for pathways with pscores bellow threshold

        """
        sorted_pscores = self.__get_sorted_pscores(threshold=threshold)
        pathways_ids = [pathway for pscore, pathway in sorted_pscores]
        return pathways_ids

    def genes_in_significant_pathways(self, threshold=0.05):
        """Retrieves a sorted list with all the genes in pathways with pscores bellow threshold.

        Args:
            threshold: maximum value for the pscore of a pathway
        Returns:
            List of two-element tuples, where the first element corresponds to the gene score and the second element
            corresponds to the gene id

        """
        pathways_significant = self.significant_pathways(threshold=threshold)
        genes_in_pathways = list(set(chain.from_iterable(self.pathways[pathway] for pathway in pathways_significant)))
        genes_in_pathways_and_scores = sorted((self.genes_scores.get(gene,0), gene) for gene in genes_in_pathways)
        genes_in_pathways_and_scores.sort(reverse=True)

        return genes_in_pathways_and_scores

    def plot_pathways(self, threshold=0.05, ax=None):
        """Plot ranked list of pathways as an horizontal bar chart.

        X-axis corresponds to the -log10 of the pathways p-scores.

        Args:
            threshold: maximum value for the pscore of a pathway
            ax:

        Returns: instance of :class:`Figure`

        """

        sorted_pscores = self.__get_sorted_pscores()
        pathways_names_pscores_sorted = [(self.names[pathway],np.abs(np.log10(pscore)*-1))
                                         for pscore, pathway in sorted_pscores if pscore <= threshold]

        if not ax:
            ax = plt.gca()

        ax.barh(np.arange(len(pathways_names_pscores_sorted)),
                [value for name,value in pathways_names_pscores_sorted],
                 align='center')

        ax.set_yticks(np.arange(len(pathways_names_pscores_sorted)))
        ax.set_yticklabels([name for name, value in pathways_names_pscores_sorted])
        ax.invert_yaxis()
        ax.set_xlabel('-log10(pscore)')
        ax.set_title('Ranking of pathways')

        ax.tick_params(axis='x', which='both', labelbottom=True, labeltop=False)

        return ax


    def plot_genes_in_significant_pathways(self, threshold=0.05, ax=None, coverage=100):
        """Plots a heatmap of scores of genes from pathways with p-scores bellow threshold.

        Args:
            threshold: maximum value for the pscore of a pathway
            ax: a matplotlib.axes.Axes instance to which the heatmap is plotted.
            coverage: percentage covering a pathway raw score. This argument is used to select only the top ranked
            genes covering the specified percentage of each of the pathway raw scores.
        Returns:
            A matplotlib.axes.Axes instance
        """
        pathways_significant = self.significant_pathways(threshold=threshold)
        genes_in_pathways_and_scores = self.genes_in_significant_pathways(threshold=threshold)
        genes = [item[1] for item in genes_in_pathways_and_scores]

        data_matrix = np.empty((len(genes), len(pathways_significant)))
        data_matrix[:] = np.nan

        # Create dataframe filled with zeroes
        genes_in_pathways_and_scores_df = pd.DataFrame(data_matrix, index=genes, columns=pathways_significant)

        # Fill dataframe
        for pathway in pathways_significant:
            pathway_genes = list(self.pathways[pathway].flat)
            genes_in_pathways_and_scores_df.loc[pathway_genes, pathway] = list(self.genes_scores.get(gene, np.nan)
                                                                               for gene in pathway_genes)
        # Remove genes with nan values for all pathways
        genes_in_pathways_and_scores_df = genes_in_pathways_and_scores_df.loc[~genes_in_pathways_and_scores_df.isnull().all(1), :]

        pathways_significant_names = [self.names[pathway_id] for pathway_id in pathways_significant]


        if coverage<100:
            pathways_significant_scores = {pathway: self.scores.get(pathway) for pathway in pathways_significant}
            genes_in_pathways_tokeep = list()

            for pathway, score in pathways_significant_scores.items():
                genes_in_pathway = sorted((self.genes_scores.get(gene), gene) for gene in self.pathways.get(pathway) \
                                          if gene in self.genes_scores)
                genes_in_pathway.sort(reverse=True)

                score_back_to_sum = score * len(genes_in_pathway)
                cumulative_score = 0
                pathway_score_coverage = 0

                for item in genes_in_pathway:
                    if pathway_score_coverage <= coverage:
                        genes_in_pathways_tokeep.append(item[1])
                        cumulative_score += item[0]
                        pathway_score_coverage = (cumulative_score / score_back_to_sum) * 100
                    else:
                        break

            genes_in_pathways_tokeep = list(set(genes_in_pathways_tokeep))

            filter_boolean = genes_in_pathways_and_scores_df.index.isin(genes_in_pathways_tokeep)

            genes_in_pathways_and_scores_df = genes_in_pathways_and_scores_df.loc[filter_boolean,:]

        if not ax:
            ax = plt.gca()


        im, cbar = util.heatmap(data = genes_in_pathways_and_scores_df.values,
                                row_labels = genes_in_pathways_and_scores_df.index.values,
                                col_labels=pathways_significant_names,
                                ax=ax, cbar_kw = {'shrink':0.15, 'orientation':'vertical'},
                                cmap="YlGn", cbarlabel="Gene score")

        return ax

    def genes_in_pathway(self, pathway_name, coverage=100):
        """Retrieves a sorted list with all the genes in pathways with pscores bellow threshold.

        Args:
            threshold: maximum value for the pscore of a pathway
        Returns:
            List of two-element tuples, where the first element corresponds to the gene score and the second element
            corresponds to the gene id

        """
        pathway_score_raw = self.scores.get(pathway_name)
        genes_in_pathway = sorted((self.genes_scores.get(gene), gene) for gene in self.pathways.get(pathway_name) \
                                  if gene in self.genes_scores)
        genes_in_pathway.sort(reverse=True)

        if coverage<100:
            score_back_to_sum = pathway_score_raw * len(genes_in_pathway)
            cumulative_score = 0
            pathway_score_coverage = 0
            genes_in_pathway_tokeep = list()
            for item in genes_in_pathway:
                if pathway_score_coverage <= coverage:
                    genes_in_pathway_tokeep.append(item[1])
                    cumulative_score += item[0]
                    pathway_score_coverage = (cumulative_score / score_back_to_sum) * 100
                else:
                    break

            genes_in_pathway_and_scores = sorted((self.genes_scores.get(gene,0), gene) for gene in genes_in_pathway_tokeep)
            genes_in_pathway_and_scores.sort(reverse=True)
            return genes_in_pathway_and_scores
        else:
            return genes_in_pathway

def score_pathways(global_graph, graph_kernel, pathways, names, samples=None, threshold=1, permutations=100):
    """Computes relevance scores for pathways given a :class:`GlobalGraph` object and its :class:`GraphKernel` object.
    Args:
        global_graph: a graph  of class GlobalGraph
        graph_kernel: the corresponding kernel of global_graph. It's an object of class GraphKernel.
        pathways: a dictionary of pathways
        names:
        samples: list of samples names for which to compute the pathway scores
        threshold: minimum number of genes in pathway to compute score
        permutations: number of permutations to compute the pathway scores
    Returns:
        Dictionary of pathways and their corresponding scores
    """

    gene_indices = global_graph.genes_indices
    gene_names = global_graph.genes_names

    if samples is not None:
        sample_indices = global_graph.indices(names=samples)
    else:
        sample_indices = global_graph.samples_indices

    # Extract similarities between a sample and a gene from kernel matrix
    per_gene_mean_similarities = graph_kernel.subset(rows=sample_indices,cols=gene_indices).mean(0)
    # indices of gene in the sample x gene similarity matrix
    gene_names_ix = {gene: ix for ix, gene in enumerate(gene_names)}

    my_pathways = pathways
    scores_for_pathways = dict()
    p_scores_for_pathways = dict()
    scores_per_gene = dict(zip(gene_names, list(per_gene_mean_similarities.flat)))

    progress_counter = 0
    for pathway in my_pathways:
        progress_counter += 1

        if progress_counter % 10 == 0:
            print('_')

        similarities_permutations = np.zeros(permutations + 1)
        genes_from_pathway = my_pathways[pathway]

        if len(genes_from_pathway) >= threshold:
            genes_in_path_indices = sorted(gene_names_ix[gene] for gene in genes_from_pathway if gene in gene_names_ix)
            gene_indices_to_shuffle = np.arange(len(gene_names_ix))

            # Compute score only if at least one gene in the pathway
            # is present in the kernel  matrix
            if len(genes_in_path_indices) >= 1:
                for permutation in range(permutations + 1):
                    if permutation == 0:
                        scores_for_pathways[pathway] = per_gene_mean_similarities[:, genes_in_path_indices].mean()
                        similarities = per_gene_mean_similarities[:, genes_in_path_indices]
                    elif permutation > 0:
                        random.shuffle(gene_indices_to_shuffle)
                        similarities = per_gene_mean_similarities[:,gene_indices_to_shuffle[0:len(genes_in_path_indices)]]

                    similarities_permutations[permutation] = similarities.mean()

                p_scores_for_pathways[pathway] = ((similarities_permutations[1:] >= similarities_permutations[0]).sum() + 1) / \
                                  (permutations + 1)

            else:
                print('{} : no genes found in kernel matrix'.format(names[pathway]))
                scores_for_pathways[pathway] = 0
                p_scores_for_pathways[pathway] = 1
        else:
            scores_for_pathways[pathway] = np.nan
            p_scores_for_pathways[pathway] = np.nan

    return PathwaysScores(pathways=my_pathways, names=names, scores=scores_for_pathways,
                          p_scores=p_scores_for_pathways, genes_scores=scores_per_gene)


def random_means_for_subset(similarity_matrix, subset_size, permutations, nr_cores=None):
    """
    Args:
        similarity_matrix:
        subset_size:
        permutations:
        nr_cores:
    Returns:

    """

    # TODO: incorporate this function in score_pathways()

    nr_of_genes_in_similarity_matrix = similarity_matrix.shape[1]
    random_genes_indices = [list(np.random.choice(nr_of_genes_in_similarity_matrix, subset_size,
                                                  replace=False)) for _ in range(permutations)]

    similarities_permutations = [similarity_matrix[:, permuted_indices].mean() for permuted_indices in random_genes_indices]

    return similarities_permutations
















