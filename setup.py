#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""The setup script."""

from setuptools import setup, find_packages

with open('README.rst') as readme_file:
    readme = readme_file.read()

with open('HISTORY.rst') as history_file:
    history = history_file.read()

requirements = [
    'Click>=6.0',
    'scipy>=1.0.0',
    'python-igraph==0.7.1.post6',
    'numpy>=1.13.3',
    'pandas>=0.21.0',
    'matplotlib>=2.2.2'
]

setup_requirements = [
    # TODO(mticlla): put setup requirements (distutils extensions, etc.) here
]

test_requirements = [
    'tornado>=5.0.2',
    'pytest>=3.2.3',
]

setup(
    name='pathways',
    version='0.1.0',
    description="Pathways allows you to compute importance scores for a a list of metabolic pathways.",
    long_description=readme + '\n\n' + history,
    author="Monica R. Ticlla Ccenhua",
    author_email='monica.ticlla@sib.swiss',
    url='https://git.scicore.unibas.ch/TBRU/pathways.git',
    packages=find_packages(include=['pathways']),
    entry_points={
        'console_scripts': [
            'pathways=pathways.cli:main'
        ]
    },
    include_package_data=True,
    install_requires=requirements,
    license="MIT license",
    zip_safe=False,
    keywords='pathways',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Natural Language :: English',
        "Programming Language :: Python :: 2",
        'Programming Language :: Python :: 2.6',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
    ],
    test_suite='tests',
    tests_require=test_requirements,
    setup_requires=setup_requirements,
)
