alabaster==0.7.10
argh==0.26.2
Babel==2.5.1
backcall==0.1.0
bcbio-gff==0.6.4
biopython==1.72
bleach==2.1.3
bumpversion==0.5.3
certifi==2018.4.16
cffi==1.11.2
chardet==3.0.4
click==6.7
coverage==4.1
cryptography==1.7
cycler==0.10.0
decorator==4.1.2
docutils==0.14
entrypoints==0.2.3
flake8==2.6.0
html5lib==1.0.1
idna==2.6
igraph==0.1.11
imagesize==0.7.1
ipykernel==4.8.2
ipython==6.4.0
ipython-genutils==0.2.0
ipywidgets==7.2.1
jedi==0.12.0
Jinja2==2.9.6
jsonschema==2.6.0
jupyter==1.0.0
jupyter-client==5.2.3
jupyter-console==5.2.0
jupyter-core==4.4.0
kiwisolver==1.0.1
llvmlite==0.23.2
MarkupSafe==1.0
matplotlib==2.2.2
mccabe==0.5.3
mistune==0.8.3
nbconvert==5.3.1
nbformat==4.4.0
networkx==2.0
nose==1.3.7
notebook==5.5.0
numba==0.38.1
numpy==1.13.3
packaging==17.1
pandas==0.21.0
pandocfilters==1.4.2
parso==0.2.1
pathtools==0.1.2
pexpect==4.6.0
pickleshare==0.7.4
plotly==2.7.0
pluggy==0.3.1
prompt-toolkit==1.0.15
ptyprocess==0.5.2
py==1.4.34
pyasn1==0.3.7
pycodestyle==2.0.0
pycparser==2.18
pyflakes==1.2.3
Pygments==2.2.0
pyparsing==2.2.0
pytest==3.2.3
python-dateutil==2.6.1
python-igraph==0.7.1.post6
pytz==2017.3
PyYAML==3.11
pyzmq==17.0.0
qtconsole==4.3.1
requests==2.19.1
scipy==1.0.0
Send2Trash==1.5.0
simplegeneric==0.8.1
six==1.11.0
snowballstemmer==1.2.1
Sphinx==1.7.6
sphinxcontrib-websupport==1.1.0
terminado==0.8.1
testpath==0.3.1
tornado==5.0.2
tox==2.3.1
traitlets==4.3.2
urllib3==1.23
virtualenv==15.1.0
watchdog==0.8.3
wcwidth==0.1.7
webencodings==0.5.1
widgetsnbextension==3.2.1
