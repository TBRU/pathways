.. highlight:: shell

============
Installation
============


Stable release
--------------

To install pathways, run this command in your terminal:

.. code-block:: console

    $ pip install pathways

This is the preferred method to install pathways, as it will always install the most recent stable release.

If you don't have `pip`_ installed, this `Python installation guide`_ can guide
you through the process.

.. _pip: https://pip.pypa.io
.. _Python installation guide: http://docs.python-guide.org/en/latest/starting/installation/


From sources
------------

The sources for pathways can be downloaded from the `GitLab repo`_.

You can either clone the public repository:

.. code-block:: console

    $ git clone https://git.scicore.unibas.ch/TBRU/pathways.git

Or download the `tarball`_:

.. code-block:: console

    $ curl  -OL https://git.scicore.unibas.ch/TBRU/pathways/-/archive/master/pathways-master.tar.gz

Once you have a copy of the source, you can install it with:

.. code-block:: console

    $ python setup.py install


.. _GitLab repo: https://git.scicore.unibas.ch/TBRU/pathways
.. _tarball: https://git.scicore.unibas.ch/TBRU/pathways/-/archive/master/pathways-master.tar.gz
