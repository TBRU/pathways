========
Pathways [a.k.a GRanMPa (Graph-based Ranking of Mutated Pathways)]
========


.. image:: https://img.shields.io/pypi/v/pathways.svg
        :target: https://pypi.python.org/pypi/pathways

.. image:: https://img.shields.io/travis/mticlla/pathways.svg
        :target: https://travis-ci.org/mticlla/pathways

.. image:: https://readthedocs.org/projects/pathways/badge/?version=latest
        :target: https://pathways.readthedocs.io/en/latest/?badge=latest
        :alt: Documentation Status

.. image:: https://pyup.io/repos/github/mticlla/pathways/shield.svg
     :target: https://pyup.io/repos/github/mticlla/pathways/
     :alt: Updates


Pathways computes importance scores for a a list of metabolic pathways.

Pathways is an adaptation of `Verbeke et al.'s`_ approach that ranks pathways by integrating multiple types of
data (gene expression, mutation, methylation and copy number data) together with a gene interaction network. Pathways is
focused on uniquely using mutation data to rank the importance of a set of genes (i.e pathways) for a group of
samples(i.e genomes).


* Free software: MIT license
* Documentation: https://pathways.readthedocs.io.


Features
--------
* TODO


Installation
------------

It's recommended to install `pathways` into a virtual environment:

1. Clone the public repository::

    $ git clone https://git.scicore.unibas.ch/TBRU/pathways.git

   or::

    $ git clone ssh://git@git.scicore.unibas.ch:2222/TBRU/pathways.git

2. Assuming you have Anaconda2 installed, with Conda, create a new environment with the Intel distribution for Python 3.6::

    $ conda config --add channels intel
    $ conda create -n pathways -c intel python=3.6 icu=58 scipy numpy pandas matplotlib

   PS: The conda environment should be stored at $HOME/.conda/envs

3. Activate the environment and install python-igraph package from conda-forge::

    $ source activate pathways
    $ conda install -c conda-forge python-igraph pycairo

4. Install `pathways`::

    $ cd pathways
    $ python setup.py install

   or::

    $ pip install

Documentation
-------------

For now (requires flake8)::

    $ make docs

Credits
-------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _`Verbeke et al.'s`: http://journals.plos.org/plosone/article?id=10.1371/journal.pone.0133503
.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage

